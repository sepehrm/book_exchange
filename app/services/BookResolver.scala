package services

import play.api.libs.json._
import play.api._
import play.api.mvc._
import play.api.libs.ws.WS
import play.api.libs.concurrent.Execution.Implicits._
import models.{ Book, Books }
import scala.concurrent.{ Await, Future }
import scala.concurrent.duration._
import akka.util.Timeout
import scala.language.postfixOps

object BookResolver {

  def findByISBN(isbn: String): Option[Book] = {
    fetchFromGoogleBooks(isbn)
  }

  def fetchFromGoogleBooks(isbn: String): Option[Book] = {
    import models.formats.Formats.GoogleBookFormat
    implicit val timeout = Timeout(50000 milliseconds)
    val url = "https://www.googleapis.com/books/v1/volumes"
    val data = WS.url(url).withQueryString(("q", "isbn:%s" format isbn)).get()
    val future = data map {
      response => response.json
    }
    val jsonResponse = Await.result(future, timeout.duration).asInstanceOf[play.api.libs.json.JsObject]
    if ((jsonResponse \ "totalItems").as[Int] > 0) {
      val volumeInfo = jsonResponse.\("items")(0).\("volumeInfo")
      val book = Json.fromJson(volumeInfo).get
      Some(book)
    } else None

  }
}