package services

import models._
import org.joda.time.DateTime
import com.github.cleverage.elasticsearch.ScalaHelpers._
import org.elasticsearch.index.query.QueryBuilders
import com.github.cleverage.elasticsearch.IndexClient.client
import org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder
import org.elasticsearch.index.query.FilterBuilders.boolFilter
import org.elasticsearch.index.query.FilterBuilders.geoDistanceFilter
import org.elasticsearch.index.query.FilterBuilders.rangeFilter
import org.elasticsearch.index.query.QueryBuilders.constantScoreQuery
import org.elasticsearch.node.NodeBuilder.nodeBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.action.search.SearchType
import org.elasticsearch.index.query.FilterBuilders._
import org.elasticsearch.index.query.QueryBuilders._
import java.io.IOException
import java.util.Date
import org.elasticsearch.action.get.GetResponse
import org.elasticsearch.action.percolate.PercolateResponse
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.Client
import org.elasticsearch.common.unit.DistanceUnit
import org.elasticsearch.common.xcontent.XContentBuilder
import org.elasticsearch.index.query.FilterBuilder
import org.elasticsearch.node.Node
import scala.collection.JavaConversions._
import org.elasticsearch.common.xcontent.XContentFactory
import org.elasticsearch.index.query.FilterBuilders
import play.api.Logger
import scala.util.Success
import scala.util.Try
import scala.util.Failure

object ElasticSearchService {

	val indexName = "campus"
	val percolatorIndex = ".percolator"
	  
	private val logger = Logger(this.getClass)

	def addAlert(instituteId: String, bookId: String, userid: String): Try[Boolean] = {

		val queryName = instituteId + "|" + bookId + "|" + userid

		// register percolator query
		val percolateQuery = FilterBuilders.boolFilter()
		.must(FilterBuilders.termFilter("bookId", bookId))
		.must(FilterBuilders.termFilter("campusId", instituteId))

		val json = jsonBuilder()
			.startObject()
			.field("query", percolateQuery) // Register the query
			.field("userId", userid)
			.field("type", "percolateDoc")
			.endObject()
		
	    //Index the query = register it in the percolator
		val response = client.prepareIndex(indexName, percolatorIndex, queryName)
			.setSource(json)
			.setRefresh(true) // Needed when the query shall be available immediately
			.execute().actionGet()
				
		Logger.debug("registered percolator query with name: " + queryName)
		Logger.debug("for query document: " + json.string())
		if (response.isCreated()) Success(true) else Success(false)
		
	}

	def getAlerts(instituteId: String, bookId: String): List[String] = {
		  	  
	    Logger.debug("searching for percolators for bookid " + bookId + " for campus " + instituteId)
	    //Build a document to check against the percolator
	    val docBuilder:XContentBuilder = XContentFactory.jsonBuilder().startObject()
	    docBuilder.field("doc").startObject()
	    docBuilder.field("bookId", bookId)
	    docBuilder.field("campusId", instituteId)
	    docBuilder.endObject()
	    docBuilder.endObject()

	    Logger.debug("using doc: " + docBuilder.string())
	
	    //Percolate
	    val response:PercolateResponse = client.preparePercolate()
	    		.setIndices(indexName)
	    		.setDocumentType("percolateDoc")
	    		.setSource(docBuilder).execute().actionGet()
	
	    Logger.debug("Percolate response count:" + response.getCount())

	    response.getMatches().toList.map(pMatch => {
	    	//Handle the result which is the name of the query in the percolator
	    	val matchName = pMatch.getId().toString()
	    	Logger.debug("matched query's name: " + matchName)
	    	
	    	Logger.debug("index: " + pMatch.getIndex().toString());
	    	Logger.debug("queryname: " + pMatch.getId().toString());
	    	
	    	val matchedQuery = client.prepareGet(indexName,percolatorIndex, matchName)
	    			.setFetchSource("userId", "query")
	    			.execute().actionGet()
	    	
	    	val userId = matchedQuery.getSource().get("userId").toString()
	    	Logger.debug("userId: " + userId + " should be notified for bookId:" + bookId)
	    	userId
	    	  
	    })
	
	}

	def deleteAlert(instituteId: String, bookId: String, userid: String): Try[Boolean] = {
	  val queryName = instituteId + "|" + bookId + "|" + userid
	  val response = client.prepareDelete(indexName, percolatorIndex, queryName).execute().actionGet()
	  if (response.isFound()) Success(true) else Success(false)
	}
	
	def hasAlert(instituteId: String, bookId: String, userid: String): Try[Boolean] = {
	  val queryName = instituteId + "|" + bookId + "|" + userid
	  val response = client.prepareGet(indexName, percolatorIndex, queryName).execute().actionGet()
	  if (response.isExists()) Success(true) else Success(false)
	}

}