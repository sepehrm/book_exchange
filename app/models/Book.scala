package models

import services.BookResolver
import com.github.cleverage.elasticsearch.ScalaHelpers._
import org.joda.time.DateTime
import org.elasticsearch.index.query.QueryBuilders


case class Book(id: String,
  isbn10: Option[String],
  isbn13: Option[String],
  title: String,
  authors: List[String],
  description: Option[String],
  publisher: Option[String],
  datePublished: Option[DateTime],
  language: Option[String],
  pageCount: Option[Int],
  edition: Option[String],
  coverPictureUrl: Option[String],
  dateCreated: DateTime,
  dateModified: DateTime) extends Indexable

object Books extends IndexableManager[Book] {
  import play.api.libs.json._

  val indexType = "book"
  val reads: Reads[Book] = Json.reads[Book]
  val writes: Writes[Book] = Json.writes[Book]

  def findByISBN(isbn: String): Option[Book] = {
    val indexQuery = IndexQuery[Book]()
      .withBuilder(QueryBuilders.multiMatchQuery(isbn, "isbn10", "isbn13"))
    val results: IndexResults[Book] = search(indexQuery)
    if (results.totalCount > 0) Some(results.results(0)) else {
      BookResolver.findByISBN(isbn) match {
        case Some(newBook) => {
          Books.index(newBook)
          Some(newBook)
        }
        case None => None
      }
    }
  }

  def find(query: String): List[Book] = {
    val indexQuery = IndexQuery[Book]()
      .withBuilder(QueryBuilders.queryString(query))
    val results: IndexResults[Book] = search(indexQuery)
    if (results.totalCount > 0) results.results else Nil
  }

  def findAll(): List[Book] = {
    val indexQuery = IndexQuery[Book]()
      .withBuilder(QueryBuilders.matchAllQuery())
    val results: IndexResults[Book] = search(indexQuery)
    if (results.totalCount > 0) results.results else Nil
  }
  

}