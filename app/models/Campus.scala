package models

import com.github.cleverage.elasticsearch.ScalaHelpers._
import org.elasticsearch.index.query.QueryBuilders

case class Campus(id: String,
  shortName: String,
  fullName: String,
  city: String,
  country: String) extends Indexable

object Campuses extends IndexableManager[Campus] {
  import play.api.libs.json._

  val indexType = "institute"
  val reads: Reads[Campus] = Json.reads[Campus]
  val writes: Writes[Campus] = Json.writes[Campus]

  def findAll(): List[Campus] = {
    val indexQuery = IndexQuery[Campus]()
      .withBuilder(QueryBuilders.matchAllQuery())
    val results: IndexResults[Campus] = search(indexQuery)
    results.results
  }

  def find(query: String): List[Campus] = {
    val indexQuery = IndexQuery[Campus]()
      .withBuilder(QueryBuilders.queryString(query))
    val results: IndexResults[Campus] = search(indexQuery)
    if (results.totalCount > 0) results.results else Nil
  }

  def findByName(name: String): Option[Campus] = {
    val indexQuery = IndexQuery[Campus]()
      .withBuilder(QueryBuilders.multiMatchQuery(name, "shortName", "fullName"))
    val results: IndexResults[Campus] = search(indexQuery)
    if (results.totalCount > 0) Some(results.results(0)) else None
  }

}
