package models

import utils.Utils
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat
import org.apache.commons.codec.binary.Base64
import java.util.UUID
import java.nio.ByteBuffer


object Defaults {

  val GoogleBooksDatePattern = "yyyy-MM-dd"
  val GoogleBooksShortDatePattern = "yyyy"
  val GoogleBooksDateTimeFormat = DateTimeFormat.forPattern(GoogleBooksDatePattern)
  val GoogleBooksShortDateTimeFormat = DateTimeFormat.forPattern(GoogleBooksShortDatePattern)
  
  val fmt = ISODateTimeFormat.dateTime()

  val GoogleBookUrl = "https://www.googleapis.com/books/v1/volumes"

  def uuid = UUID.randomUUID.toString
  def uid = uuidToBase64(UUID.randomUUID.toString)
  def now = DateTime.now(DateTimeZone.UTC)
  
  def uuidToBase64(str: String) = Utils.uuidToBase64(str)
  
  def uuidFromBase64(str: String): String = Utils.uuidFromBase64(str)

}
