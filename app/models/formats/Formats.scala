package models.formats

import models._
import play.api.libs.json._
import org.joda.time.format.DateTimeFormatter

/**
 * Json format for various objects
 */

object Formats {

  implicit val courseFormat = Json.format[Course]
  implicit val bookFormat = Json.format[Book]
  implicit val campusFormat = Json.format[Campus]
  implicit val bookPostFormat = Json.format[BookPost]

  implicit object GoogleBookFormat extends Reads[Book] {

    import Defaults._

    def reads(volumeInfo: JsValue) = {
      val title = (volumeInfo \ "title").asOpt[String]
      val authors = (volumeInfo \ "authors").asOpt[List[String]]
      val description = (volumeInfo \ "description").asOpt[String]

      val publisher = (volumeInfo \ "publisher").asOpt[String]
      val datePublished = (volumeInfo \ "publishedDate") match {
        case JsString(str) if str.length == GoogleBooksDatePattern.length => Some(GoogleBooksDateTimeFormat.parseDateTime(str))
        case JsString(str) if str.length == GoogleBooksShortDatePattern.length => Some(GoogleBooksShortDateTimeFormat.parseDateTime(str))
        case _ => None
      }
      val language = (volumeInfo \ "language").asOpt[String]
      val imgUrl = (volumeInfo \ "imageLinks" \ "thumbnail").asOpt[String]
      val pageCount = (volumeInfo \ "pageCount").asOpt[Int]
      val edition = None

      val isbn10 = (volumeInfo \ "industryIdentifiers")(0).\("identifier").asOpt[String]
      val isbn13 = (volumeInfo \ "industryIdentifiers")(1).\("identifier").asOpt[String]

      JsSuccess(Book(Defaults.uuid, isbn10, isbn13, title.getOrElse(""), authors.getOrElse(List()),
        description, publisher, datePublished, language, pageCount, edition, imgUrl, Defaults.now, Defaults.now))

    }
  }

}