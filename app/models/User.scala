package models

import services.ElasticSearchService
import com.github.cleverage.elasticsearch.ScalaHelpers._
import org.elasticsearch.index.query.QueryBuilders

case class User(id: String,
    defaultInstituteId: String, 
    token: String) extends Indexable

object Users extends IndexableManager[User] {
  import play.api.libs.json._
  
  val indexType = "user"
  val reads: Reads[User] = Json.reads[User]
  val writes: Writes[User] = Json.writes[User]
  
  def findById(id: String): Option[User] = {
    val indexQuery = IndexQuery[User]()
    	.withBuilder(QueryBuilders.matchQuery("id", id))
    val results: IndexResults[User] = search(indexQuery)
    if (results.totalCount > 0) Some(results.results(0)) else None
  }
  
  def hasAlert(instName: String, isbn: String, userId: String) = {
    val institute = Campuses.findByName(instName).get
    val book = Books.findByISBN(isbn).get
    val user = Users.findById(userId).get
    ElasticSearchService.hasAlert(institute.id, book.id, user.id)
  }
  
  def addAlert(instName: String, isbn: String, userId: String) = {
    //TODO all options, verify existence
    val institute = Campuses.findByName(instName).get
    val book = Books.findByISBN(isbn).get
    val user = Users.findById(userId).get
    ElasticSearchService.addAlert(institute.id, book.id, user.id)
  }
  
  def deleteAlert(instName: String, isbn: String, userId: String) = {
    //TODO all options, verify existence
    val institute = Campuses.findByName(instName).get
    val book = Books.findByISBN(isbn).get
    val user = Users.findById(userId).get
    ElasticSearchService.deleteAlert(institute.id, book.id, user.id)
  }

}
