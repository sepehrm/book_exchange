package models

import com.github.cleverage.elasticsearch.ScalaHelpers._
import org.elasticsearch.index.query.QueryBuilders

case class Course(id: String,
  instId: String,
  code: String,
  title: Option[String],
  description: Option[String]) extends Indexable

object Courses extends IndexableManager[Course] {
  import play.api.libs.json._

  val indexType = "course"
  val reads: Reads[Course] = Json.reads[Course]
  val writes: Writes[Course] = Json.writes[Course]

  def findByCode(instName: String, code: String): Option[Course] = {
    val indexQuery = IndexQuery[Course]()
    	.withBuilder(QueryBuilders.matchQuery("institute.shortName", instName))
    	    .withBuilder(QueryBuilders.matchQuery("code", code))
    val results: IndexResults[Course] = search(indexQuery)
    if (results.totalCount > 0) Some(results.results(0)) else {
		val newCourse = Course(
		Defaults.uuid, Campuses.findByName(instName).get.id,
		code,
		None,
		None)
		this.index(newCourse)
		Some(newCourse)
    }
  }

}
