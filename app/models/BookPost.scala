package models

import com.github.cleverage.elasticsearch.ScalaHelpers._
import org.joda.time.DateTime
import org.elasticsearch.index.query.QueryBuilders
import services.BookResolver
import services.ElasticSearchService

case class SimpleBookPost(
  userId: String,
  courseCode: Option[String],
  description: Option[String],
  price: Option[String])

case class BookPost(id: String,
  userId: String,
  book: Book,
  course: Option[Course],
  campus: Campus,
  description: Option[String],
  price: Option[String],
  datePosted: DateTime) extends Indexable

object BookPosts extends IndexableManager[BookPost] {
  import play.api.libs.json._
  implicit val courseFormat = Json.format[Course]
  implicit val bookFormat = Json.format[Book]
  implicit val campusFormat = Json.format[Campus]
  implicit val bookPostFormat = Json.format[BookPost]

  val indexType = "bookpost"
  val reads: Reads[BookPost] = Json.reads[BookPost]
  val writes: Writes[BookPost] = Json.writes[BookPost]
  
  def baseQueryBuilder(instName: String) = QueryBuilders.termQuery("institute.shortName", instName)

  def findAll(instName: String): List[BookPost] = {
    val indexQuery = IndexQuery[BookPost]()
    	.withBuilder(QueryBuilders.matchQuery("institute.shortName", instName))
    val results: IndexResults[BookPost] = search(indexQuery)
    if (results.totalCount > 0) results.results else Nil
  }

  def find(instName: String, query: String): List[BookPost] = {
    val indexQuery = IndexQuery[BookPost]()
    	.withBuilder(QueryBuilders.matchQuery("institute.shortName", instName))
    	    .withBuilder(QueryBuilders.queryString(query))
    val results: IndexResults[BookPost] = search(indexQuery)
    if (results.totalCount > 0) results.results else Nil
  }

  def findByISBN(instName: String, isbn: String): List[BookPost] = {
    val indexQuery = IndexQuery[BookPost]()
    	.withBuilder(QueryBuilders.matchQuery("institute.shortName", instName))
    	    .withBuilder(QueryBuilders.multiMatchQuery(isbn, "book.isbn10", "book.isbn13"))
    val results: IndexResults[BookPost] = search(indexQuery)
    if (results.totalCount > 0) results.results else Nil
  }

  def findById(postId: String): List[BookPost] = {
    val indexQuery = IndexQuery[BookPost]()
      .withBuilder(QueryBuilders.matchQuery("id", postId))
    val results: IndexResults[BookPost] = search(indexQuery)
    if (results.totalCount > 0) results.results else Nil
  }
  
  def getAlerts(bookPost: BookPost) = {
    val userstoAlert = ElasticSearchService.getAlerts(bookPost.campus.id, bookPost.book.id)
  }

  def fromSimplePost(instName: String, isbn: String, simpleBookPost: SimpleBookPost): Option[BookPost] = {
    
    val userId = simpleBookPost.userId
    val book = Books.findByISBN(isbn)
    val course = simpleBookPost.courseCode.flatMap(code => Courses.findByCode(instName, code))
    val institute = Campuses.findByName(instName) 
    institute match {
      case Some(_) => Some(BookPost(Defaults.uuid, userId, book.get, course, institute.get, simpleBookPost.description, simpleBookPost.price, Defaults.now))
      case None => None
    }
  }

}