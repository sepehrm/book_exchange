package controllers

import play.api.mvc.{ Action, Controller }
import play.api.libs.json._

import models.formats.Formats._
import models.{ Campus, Campuses }

object CampusController extends Controller {

  def list() = Action { implicit request =>
    val results = Campuses.findAll
    Ok(Json.toJson(results))
  }

  def find(query: String) = Action { implicit request =>
    val results = Campuses.find(query)
    Ok(Json.toJson(results))
  }

  def details(name: String) = Action { implicit request =>
    Redirect(routes.BookPostController.list(name))
  }

}
