package controllers

import play.api.mvc.{ Action, Controller }
import play.api.libs.json._
import models.formats.Formats._
import models.{ Book, Books }

import com.github.cleverage.elasticsearch.ScalaHelpers._

object BookController extends Controller {

  def list() = Action { implicit request =>
    val results = Books.findAll
    results match {
      case Nil => NotFound
      case _ => Ok(Json.toJson(results))
    }
  }

  def findByISBN(isbn: String) = Action { implicit request =>
    Books.findByISBN(isbn).map { book =>
      Ok(Json.toJson(book))
    }.getOrElse(NotFound)
  }

  def find(query: String) = Action { implicit request =>
    val results = Books.find(query)
    results match {
      case Nil => NotFound
      case _ => Ok(Json.toJson(results))
    }
  }
}
