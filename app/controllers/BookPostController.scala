package controllers

import play.api.mvc.{ Action, Controller }
import play.api.libs.json._

import models.formats.Formats._
import models.{BookPost, BookPosts, SimpleBookPost }

object BookPostController extends Controller {

  def list(instName: String) = Action { implicit request =>
    val results = BookPosts.findAll(instName)
    results match {
      case Nil => NotFound
      case _ => Ok(Json.toJson(results))
    }
  }

  def find(instName: String, query: String) = Action { implicit request =>
    val results = BookPosts.find(instName, query)
    results match {
      case Nil => NotFound
      case _ => Ok(Json.toJson(results))
    }
  }

  def findByISBN(instName: String, isbn: String) = Action { implicit request =>
    val results = BookPosts.findByISBN(instName, isbn)
    results match {
      case Nil => NotFound
      case _ => Ok(Json.toJson(results))
    }
  }

  def findById(instName: String, isbn: String, postId: String) = Action { implicit request =>
    val results = BookPosts.findById(postId)
    results match {
      case Nil => NotFound
      case _ => Ok(Json.toJson(results))
    }
  }

  def save(instName: String, isbn: String) = Action(parse.json) { request =>
    implicit val courseFormat = Json.format[SimpleBookPost]
    val bookPostJson = request.body
    val simpleBookPost = bookPostJson.as[SimpleBookPost]

    val bookPost = BookPosts.fromSimplePost(instName, isbn, simpleBookPost)

    bookPost match {
      case Some(bookPost: BookPost) => {
        BookPosts.index(bookPost)

        println(Json.toJson(bookPost))
        Ok(Json.toJson(bookPost)) 
      }
      case None => {
        BadRequest("institute not found")
      }
    }

    
  }

}
