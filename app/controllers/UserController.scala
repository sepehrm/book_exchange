package controllers

import play.api.mvc.{ Action, Controller }
import play.api.libs.json._
import models.formats.Formats._
import models.{ User, Users }
import scala.util.Failure
import scala.util.Success

case class SimpleResponse(success: String, resource: String)

object UserController extends Controller {
  
  implicit val formats: Format[SimpleResponse] = Json.format[SimpleResponse]

  def hasAlert(instName: String, isbn: String, userId: String) = Action { request =>
    val results = Users.hasAlert(instName, isbn, userId)
    results match {
      case Failure(e) => Ok(Json.toJson(SimpleResponse("false", "")))
      case Success(r) => r match {
	      case true => Ok(Json.toJson(SimpleResponse("true", "true")))
	      case false => Ok(Json.toJson(SimpleResponse("true", "false")))
	  }
    }
  }
  
  def addAlert(instName: String, isbn: String, userId: String) = Action { request =>
    val results = Users.addAlert(instName, isbn, userId)
    results match {
      case Failure(e) => Ok(Json.toJson(SimpleResponse("false", "")))
      case Success(r) => r match {
	      case true => Ok(Json.toJson(SimpleResponse("true", "true")))
	      case false => Ok(Json.toJson(SimpleResponse("true", "false")))
	  }
    }
  }
  
  def deleteAlert(instName: String, isbn: String, userId: String) = Action { request =>
    val results = Users.deleteAlert(instName, isbn, userId)
    results match {
      case Failure(e) => Ok(Json.toJson(SimpleResponse("false", "")))
      case Success(r) => r match {
	      case true => Ok(Json.toJson(SimpleResponse("true", "true")))
	      case false => Ok(Json.toJson(SimpleResponse("true", "false")))
	  }
    }
  }
}
