import play.api.GlobalSettings
import models._
import services._
import play.api.Application
//import play.api.Play.current
//import play.api.Play

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    seed()
  }

  override def onStop(app: Application) {

  }

  private def seed() = {
    
    val campuses = List(
      Campus(Defaults.uuid, "university-of-toronto-st.george", "University of Toronto - St. George", "Toronto", "Canada"),
      Campus(Defaults.uuid, "ryerson", "Ryerson University", "Toronto", "Canada"))
      
    val users = List(
        User("sepehr", campuses(0).id, "sepehrs_token"),
        User("maryam", campuses(0).id, "maryams_token")
        )

    val courses = List(
      Course(
        Defaults.uuid, campuses(0).id,
        "CSC148",
        Some("Introduction to Computer Science"),
        Some("Abstract data types and data structures for implementing them. Linked data structures. Encapsulation and information-hiding. Object-oriented programming. Specifications. Analyzing the efficiency of programs. Recursion.")),
      Course(
        Defaults.uuid, campuses(0).id,
        "PSY100",
        Some("Introductory Psychology"),
        Some("A brief introductory survey of psychology as both a biological and social science. Topics will include physiological, learning, perception, motivation, cognition, developmental, personality, abnormal, and social psychology.")))

    val geneticsBook = BookResolver.fetchFromGoogleBooks("9780716799023")
    val psychologyBook = BookResolver.fetchFromGoogleBooks("9780132918350")
    
    val books = List(
      geneticsBook, psychologyBook)

    val bookPosts = List(
      BookPost(
        Defaults.uuid, users(0).id, books(0).get, Some(courses(1)), campuses(0),
        Some("Mint condition"), Some("Negotiable"), Defaults.now),
      BookPost(
        Defaults.uuid, users(1).id, books(1).get, Some(courses(0)), campuses(0),
        Some("with highlights"), Some("$120"), Defaults.now))

    users.foreach(Users.index)
    campuses.foreach(Campuses.index)
    courses.foreach(Courses.index)
    books.flatMap(xs => xs.map(Books.index))
    
    Thread sleep 10000
    Users.addAlert(campuses(0).shortName, books(1).get.isbn13.get, users(0).id)
    Users.addAlert(campuses(0).shortName, books(0).get.isbn13.get, users(1).id)
    
    bookPosts.foreach(BookPosts.index)
    bookPosts.foreach(BookPosts.getAlerts)

  }
}