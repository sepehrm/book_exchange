name := """kampuso"""

version := "1.0-SNAPSHOT"

scalacOptions ++= Seq("-unchecked", "-deprecation","-feature")

libraryDependencies ++= Seq(
  // Select Play modules
  //jdbc,      // The JDBC connection pool and the play.api.db API
  //anorm,     // Scala RDBMS Library
  //javaJdbc,  // Java database API
  //javaEbean, // Java Ebean plugin
  //javaJpa,   // Java JPA plugin
  //filters,   // A set of built-in filters
  javaCore  // The core Java API
  // WebJars pull in client-side web libraries
  //"org.webjars" %% "webjars-play" % "2.2.0",
  //"org.webjars" % "bootstrap" % "2.3.1"
  // Add your own project dependencies in the form:
  // "group" % "artifact" % "version"
)

libraryDependencies += "org.elasticsearch" % "elasticsearch" % "0.90.1"

libraryDependencies += "commons-io" % "commons-io" % "2.4"

libraryDependencies += "com.clever-age" % "play2-elasticsearch" % "1.1.0"

//libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "1.2.0"

libraryDependencies += "commons-codec" % "commons-codec" % "1.9"

play.Project.playScalaSettings
